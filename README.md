# @vizworx/eslint-config-react

This package provides a config with React-specific linting rules used at VizworX, that is based upon [eslint-config-airbnb](https://npmjs.com/eslint-config-airbnb) and [@vizworx/eslint-config](https://npmjs.com/@vizworx/eslint-config).

## Install

With npm:

```sh
npm install --save-dev @vizworx/eslint-config-react
```

Or yarn:

```sh
yarn add @vizworx/eslint-config-react --dev
```

## Usage

### .eslintrc

```json
{
  "extends": ["@vizworx/eslint-config-react"]
}
```
