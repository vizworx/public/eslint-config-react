module.exports = {
  extends: ['airbnb', 'airbnb/hooks', '@vizworx/eslint-config'],
  env: { browser: true },
  rules: {
    'react/static-property-placement': [2, 'static public field'],
  },
  overrides: [
    {
      files: [
        '**/spec.jsx',
        '**/*.spec.jsx',
      ],
      env: { jest: true },
    },
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx'],
      },
    },
  },
};
